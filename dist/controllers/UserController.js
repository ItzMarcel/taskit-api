"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_errors_1 = __importDefault(require("http-errors"));
const Validator_1 = __importDefault(require("../helpers/Validator"));
const User_1 = __importDefault(require("../models/User"));
const bcrypt = __importStar(require("bcrypt"));
exports.default = new class UserController {
    SafeUserDecorator(userObject) {
        let permission = [];
        permission.push(userObject[0].permission);
        // check if there is more then 1 permission
        if (userObject.length > 1) {
            // more then 1 permission
            userObject.forEach((userRow) => {
                permission.push(userRow.permission);
            });
        }
        return {
            id: userObject[0].id,
            firstname: userObject[0].firstname,
            lastname: userObject[0].lastname,
            email: userObject[0].email,
            color: userObject[0].color,
            rolename: userObject[0].rolename,
            permission,
        };
    }
    ;
    login(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            // get user params from body
            const { email = '', password = '' } = req.body;
            // validate params
            if (!Validator_1.default.isStr(email) || Validator_1.default.isEmpty(email)) {
                return next(http_errors_1.default(400, 'Email invalid.'));
            }
            if (!Validator_1.default.isStr(password) || Validator_1.default.isEmpty(password)) {
                return next(http_errors_1.default(400, 'Password invalid.'));
            }
            // get user from database
            const usersfromdb = yield User_1.default.getUserByEmail(email);
            // no user found send error
            if (usersfromdb.length <= 0) {
                return next(http_errors_1.default(400, 'Username or password is wrong.'));
            }
            console.log('user from db', usersfromdb[0].password);
            // compare password
            bcrypt.compare(password, usersfromdb[0].password, (err, result) => {
                // if bad
                // send user bad password error
                if (err)
                    return next(http_errors_1.default(400, 'Username or password is wrong.'));
                const safeUser = this.SafeUserDecorator(usersfromdb);
                // if good
                if (!result) {
                    res.json({
                        user: safeUser,
                    });
                    // create token
                    // send token to the user
                    // send create token error to user
                }
            });
        });
    }
    ;
    register(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            // get user params from body with default empty params
            const { firstname = '', lastname = '', email = '', password = '', color = 'ffffff' } = req.body;
            console.log(req.body);
            // validate params
            if (!Validator_1.default.isStr(email) || Validator_1.default.isEmpty(email)) {
                return next(http_errors_1.default(400, 'Email invalid.'));
            }
            if (!Validator_1.default.isStr(password) || Validator_1.default.isEmpty(password)) {
                return next(http_errors_1.default(400, 'Password invalid.'));
            }
            if (!Validator_1.default.isStr(firstname) || Validator_1.default.isEmpty(firstname)) {
                return next(http_errors_1.default(400, 'Firstname invalid.'));
            }
            if (!Validator_1.default.isStr(lastname) || Validator_1.default.isEmpty(lastname)) {
                return next(http_errors_1.default(400, 'Lastname invalid.'));
            }
            if (!Validator_1.default.isStr(color) || !Validator_1.default.isLength(color, 6)) {
                return next(http_errors_1.default(400, 'Color invalid.'));
            }
            const usersfromdb = yield User_1.default.getUserByEmail(email);
            if (usersfromdb.length > 0) {
                return next(http_errors_1.default(400, 'Email already exists.'));
            }
            const userObject = {
                firstname,
                lastname,
                email,
                color,
                password,
            };
            // create user
            yield User_1.default.createUser(userObject);
            // get new user
            const newUser = yield User_1.default.getUserByEmail(email);
            const safeUser = this.SafeUserDecorator(newUser);
            res.json({
                user: safeUser,
            });
        });
    }
    ;
};
//# sourceMappingURL=UserController.js.map