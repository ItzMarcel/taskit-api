"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new class Validator {
    // if string is empty return true;
    isEmpty(str) {
        if (str.length <= 0) {
            return true;
        }
        return false;
    }
    isStr(param) {
        console.log(param, typeof param === 'string');
        return typeof param === 'string';
    }
    isLength(param, length) {
        return param.length === length;
    }
    isMinLength(param, length) {
        return param.length >= length;
    }
    isMaxLength(param, length) {
        return param.length < length;
    }
};
//# sourceMappingURL=Validator.js.map