"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = __importDefault(require("../database/Database"));
const bcrypt = __importStar(require("bcrypt"));
exports.default = new class User {
    getUserByEmail(email) {
        return new Promise((resolve, reject) => {
            const sql = `SELECT 
                  u.id,
                    u.firstname,
                    u.lastname,
                    u.email,
                    u.color,
                    u.created_at AS user_creation,
                      u.password, r.name AS rolename,
                        r.description AS role_description,
                          p.permission
                  FROM users AS u
                  INNER JOIN user_role AS ur ON u.id = ur.user_id
                  INNER JOIN roles AS r ON ur.role_id = r.id
                  LEFT JOIN role_permissions AS rp ON r.id = rp.role_id
                  LEFT JOIN permissions AS p ON rp.permissions_id = p.id
                  WHERE email = ?`;
            Database_1.default.pool.query(sql, [email], (error, results) => {
                if (error)
                    reject(error);
                resolve(results);
            });
        });
    }
    createUser({ firstname, lastname, email, password, color }) {
        return new Promise((resolve, reject) => {
            bcrypt.hash('myPassword', 10, function (err, hash) {
                if (err)
                    reject(err);
                const sql = `INSERT INTO users (firstname, lastname, email, color, created_at, password)
                    VALUES (?, ?, ?, ?, NOW(), ?);`;
                Database_1.default.pool.query(sql, [firstname, lastname, email, color, hash], (error, results) => {
                    if (error)
                        reject(error);
                    // TODO: change 2 to select for member role!
                    const rolesql = `INSERT INTO user_role (user_id, role_id, created_at)
                    VALUES (?, 2, NOW());`;
                    Database_1.default.pool.query(rolesql, [results.insertId], (err, results) => {
                        if (error)
                            reject(error);
                        resolve(results);
                    });
                });
            });
        });
    }
};
//# sourceMappingURL=User.js.map