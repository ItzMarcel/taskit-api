# TaskIt api  

## Description  
TaskIt api is the NodeJs backend for the TaskIt-vue frontend. This api is build with the Express framework and Typescript.  

## Install  
1. `git clone git@gitlab.com:ItzMarcel/taskit-api.git`  
2. `npm install`  
3. Run the MySql database sql file.  
4. Run in development mode with `npm run dev` or production mode with `npm run prod`  

## Development  
You can reach the api from `http://127.0.0.1:3000`  

## Routes  

### Route: /users  
Method: POST  

Expects to get an e-mail and a password.
```Typescript
{
  email: string;
  password: string;
}
```

#### Response with
On success
```


```