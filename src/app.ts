const createError = require('http-errors');
import express from 'express';
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const app = express();

require('dotenv').config()

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
  res.json({ 'Version': process.env.VERSION });
});
app.use('/api/v1/', require('./routes/index'));
app.use('/api/v1/users', require('./routes/users'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err: any, req: any, res: any, next: any) {
  res.status(err.status || 500);
  res.json({
    success: false,
    error: {
      status: err.status || 500,
      message: err.message,
    },
  });
});

module.exports = app;
