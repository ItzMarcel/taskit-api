import * as Express from 'express';
import createError from 'http-errors';

import Validator from '../helpers/Validator';
import User from '../models/User';
import * as bcrypt from 'bcrypt';

import {
  safeUserDecorator
} from '../helpers/decorators/UserDecorator';

import {
  IUser,
  INewUser,
  ISafeUser,
  Permission
} from '../interfaces/UserInterface';

type ResponseUserObjectType = (userObject: Array<IUser>) => ISafeUser;

class UserController {

  public async login(req: Express.Request, res: Express.Response, next: Express.NextFunction): Promise<void> {
    // get user params from body
    const { email = '', password = '' } = req.body;

    // validate params
    if (!Validator.isStr(email) || Validator.isEmpty(email)) {
      return next(createError(400, 'Email invalid.'));
    }

    if (!Validator.isStr(password) || Validator.isEmpty(password)) {
      return next(createError(400, 'Password invalid.'));
    }

    // get user from database
    const usersfromdb: Array<IUser> = await User.getUserByEmail(email);

    // no user found send error
    if (usersfromdb.length <= 0) {
      return next(createError(400, 'Username or password is wrong.'));
    }

    // compare password
    bcrypt.compare(password, usersfromdb[0].password, (err, result) => {
      // if bad
        // send user bad password error
      if (err || !result) return next(createError(400, 'Username or password is wrong.'))

      const safeUser: ISafeUser = safeUserDecorator(usersfromdb);

      // if good
      if (result) {
        res.json({
          user: safeUser,
        });

        // create token
        // send token to the user
        // send create token error to user
      }
    });
  };

  public async register(req: Express.Request, res: Express.Response, next: Express.NextFunction): Promise<void> {
    // get user params from body with default empty params
    const { firstname = '', lastname = '', email = '', password = '', color = 'ffffff' } = req.body;

    // validate params
    if (!Validator.isStr(email) || Validator.isEmpty(email)) {
      return next(createError(400, 'Email invalid.'));
    }

    if (!Validator.isStr(password) || Validator.isEmpty(password)) {
      return next(createError(400, 'Password invalid.'));
    }

    if (!Validator.isStr(firstname) || Validator.isEmpty(firstname)) {
      return next(createError(400, 'Firstname invalid.'));
    }

    if (!Validator.isStr(lastname) || Validator.isEmpty(lastname)) {
      return next(createError(400, 'Lastname invalid.'));
    }

    if (!Validator.isStr(color) || !Validator.isLength(color, 6)) {
      return next(createError(400, 'Color invalid.'));
    }

    const usersfromdb: Array<IUser> = await User.getUserByEmail(email);

    if (usersfromdb.length > 0) {
      return next(createError(400, 'Email already exists.'));
    }

    const userObject: INewUser = {
      firstname,
      lastname,
      email,
      color,
      password,
    };
    
    // create user
    await User.createUser(userObject);

    // get new user
    const newUser: Array<IUser> = await User.getUserByEmail(email);

    const safeUser: ISafeUser = safeUserDecorator(newUser);

    res.json({
      user: safeUser,
    });
  };
};

export default new UserController;