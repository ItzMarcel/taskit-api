

export default new class Validator {

  // if string is empty return true;
  isEmpty(str: string) {
    if (str.length <= 0) {
      return true;
    }
    return false;
  }

  isStr(param: any) {
    console.log(param, typeof param === 'string');
    return typeof param === 'string';
  }

  isLength(param: string, length: number) {
    return param.length === length;
  }

  isMinLength(param: string, length: number) {
    return param.length >= length;
  }

  isMaxLength(param: string, length: number) {
    return param.length < length;
  }
}