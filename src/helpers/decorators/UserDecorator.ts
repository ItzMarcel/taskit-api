import {
  IUser,
  Permission
} from '../../interfaces/UserInterface';


export const safeUserDecorator = (userObject: Array<IUser>) => {
    let permission: Permission = [];
    permission.push(userObject[0].permission);

    // check if there is more then 1 permission
    if(userObject.length > 1) {
    // more then 1 permission
    userObject.forEach((userRow: IUser) => {
      permission.push(userRow.permission);
    });
  }

  return {
    id: userObject[0].id,
    firstname: userObject[0].firstname,
    lastname: userObject[0].lastname,
    email: userObject[0].email,
    color: userObject[0].color,
    rolename: userObject[0].rolename,
    permission,
  };
};