
export interface IUser {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  color: string;
  user_creation: string;
  password: string;
  rolename: string;
  role_description?: string;
  permission: string;
}

export interface INewUser {
  firstname: string;
  lastname: string;
  email: string;
  color: string;
  password: string;
}

export interface ISafeUser {
  id: number,
  firstname: string,
  lastname: string,
  email: string,
  color: string,
  rolename: string,
  permission?: Permission;
}

export type Permission = Array<String> | null;
