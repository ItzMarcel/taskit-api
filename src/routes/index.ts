import express from 'express';
const router = express.Router();

router.get('/', (req, res, next) => {
  res.json({ 'version': process.env.VERSION });
});

module.exports = router;
