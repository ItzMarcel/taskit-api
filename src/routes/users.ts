import express from 'express';
const router = express.Router();
import uc from '../controllers/UserController';

router.post('/login', uc.login);
router.post('/register', uc.register);

module.exports = router;
